Repository ini berisi konfigurasi [Visual Studio Code](https://code.visualstudio.com/) yang biasa saya gunakan.

---

Resource yang digunakan:
  - Font [JetBrains Mono](https://www.jetbrains.com/lp/mono/)

---

Cara penggunaan:
  - Replace `settings.json` dengan yang ada di `conf/settings.json`

Untuk lokasi `settings.json` bisa melihat di [dokumentasi Visual Studio Code](https://code.visualstudio.com/docs/getstarted/settings#_settings-file-locations)
