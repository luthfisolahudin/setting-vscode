# Looks
code --install-extension antfu.icons-carbon
code --install-extension artlaman.chalice-icon-theme
code --install-extension JamesSauer.gdscript-theme
code --install-extension PKief.material-icon-theme

# Language
code --install-extension expo.vscode-expo-tools
code --install-extension kosz78.nim
code --install-extension LoyieKing.smalise
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-vscode.cpptools
code --install-extension ziglang.vscode-zig

# Tools
code --install-extension eamodio.gitlens
code --install-extension firefox-devtools.vscode-firefox-debug
code --install-extension glenn2223.live-sass
code --install-extension marp-team.marp-vscode
code --install-extension ms-vscode-remote.remote-containers
code --install-extension ms-vscode-remote.remote-wsl
code --install-extension ritwickdey.LiveServer
code --install-extension tintinweb.vscode-decompiler
code --install-extension yoavbls.pretty-ts-errors
